# Kitchen Converter

**Features:**

- Bij elke wijziging in de input verandert de outputwaarde.
- De output toont de juiste waarde, gevolgd door de eenheid.
- Gebruik de pijltjestoetsen om de invoerwaarde te verhogen of te verlagen.
- Gebruik de letters q, w, e, r om de starteenheid aan te passen.
- Gebruik de letters a, s, d, f om de gewenste eenheid aan te passen.



U kunt Kitchen Aid **[hier](https://remarkable-unicorn-9d9b0c.netlify.app/)** vinden.