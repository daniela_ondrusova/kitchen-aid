const input = document.querySelector("#invoer");
const output = document.querySelector(".output h3");
let startEenheid = document.querySelector("#startEenheid");
let convertEenheid = document.querySelector("#convertEenheid");

let factor1 = 1;
let factor = 1;

let myFunction = e => {
    if (e.key === "ArrowUp") {
        input.value = Number(input.value) + 10;
    }
    if (e.key === "ArrowDown") {
        input.value = Number(input.value) - 10;
    }
    if (e.key === "ArrowRight") {
        input.value = Number(input.value) + 1;
    }
    if (e.key === "ArrowLeft") {
        input.value = Number(input.value) - 1;
    }

    e.preventDefault();

    if (e.key === "q") {
        startEenheid.value = "ml";
    }
    if (e.key === "w") {
        startEenheid.value = "cl";
    }
    if (e.key === "e") {
        startEenheid.value = "dl";
    }
    if (e.key === "r") {
        startEenheid.value = "l";
    }

    if (e.key === "a") {
        convertEenheid.value = "ml";
    }
    if (e.key === "s") {
        convertEenheid.value = "cl";
    }
    if (e.key === "d") {
        convertEenheid.value = "dl";
    }
    if (e.key === "f") {
        convertEenheid.value = "l";
    }

    if (startEenheid.value == "l") {
        factor = 1;
    }
    if (startEenheid.value == "dl") {
        factor = 10;
    }
    if (startEenheid.value == "cl") {
        factor = 100;
    }
    if (startEenheid.value == "ml") {
        factor = 1000;
    }

    if (convertEenheid.value == "l") {
        factor1 = 1;
    }
    if (convertEenheid.value == "dl") {
        factor1 = 10;
    }
    if (convertEenheid.value == "cl") {
        factor1 = 100;
    }
    if (convertEenheid.value == "ml") {
        factor1 = 1000;
    }
    output.innerHTML = (input.value * factor1) / factor + " " + convertEenheid.value;
};

input.addEventListener("input", myFunction, false);
input.addEventListener("keydown", myFunction, false);
startEenheid.addEventListener("keydown", myFunction, false);
startEenheid.addEventListener("change", myFunction, false);
convertEenheid.addEventListener("keydown", myFunction, false);
convertEenheid.addEventListener("change", myFunction, false);